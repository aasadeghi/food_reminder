def weekday_num_to_weekday_name(num):
    if num == 0:
        return "شنبه"
    elif num == 1:
        return "یکشنبه"
    elif num == 2:
        return "دوشنبه"
    elif num == 3:
        return "سه‌شنبه"
    elif num == 4:
        return "چهارشنبه"
    elif num == 5:
        return "پنج‌شنبه"
    elif num == 6:
        return "جمعه"


def month_short_name_to_num(num):
    if num == "فرو":
        return 1
    elif num == "ارد":
        return 2
    elif num == "خرد":
        return 3
    elif num == "تیر":
        return 4
    elif num == "مرد":
        return 5
    elif num == "شهر":
        return 6
    elif num == "مهر":
        return 7
    elif num == "آبا":
        return 8
    elif num == "آذر":
        return 9
    elif num == "دی":
        return 10
    elif num == "بهم":
        return 11
    elif num == "اسف":
        return 12
    else:
        raise Exception("Wrong month number")


def get_tomorrow_weekday_num(num):
    return (num + 1) % 7
