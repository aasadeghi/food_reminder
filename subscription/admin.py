from django.contrib import admin

from subscription.models import Subscriber
from utils.models import BaseAdmin


class SubscriptionAdmin(BaseAdmin):
    list_display = [field.name for field in Subscriber._meta.fields]


admin.site.register(Subscriber, SubscriptionAdmin)
