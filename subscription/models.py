from django.core.validators import validate_email
from django.db import models

from utils.models import BaseModel


class Subscriber(BaseModel):
    telegram_id = models.BigIntegerField()
    email = models.EmailField(null=True, blank=True)
    cell_phone = models.BigIntegerField(null=True, blank=True)
    notify = models.BooleanField(default=True)

    class Meta:
        unique_together = ['email']

    def save(self, *args, **kwargs):
        if self.email is not None:
            validate_email(self.email)
        super(Subscriber, self).save(*args, **kwargs)
