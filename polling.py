from time import sleep
import django
from django.conf import settings
import os

from telegram.ext import Dispatcher
from telegram.ext.updater import Updater

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "food_reminder.settings")
django.setup()

from subscription.views import UpdateHandler

if __name__ == '__main__':
    updater = Updater(settings.TOKEN, use_context=True)
    dispatcher: Dispatcher = updater.dispatcher
    for update_handler in UpdateHandler.get_update_handlers():
        dispatcher.add_handler(update_handler)
    updater.start_polling(poll_interval=5, timeout=5)
